#!/usr/bin/env python3
import sys
import requests
import logging
from getpass import getpass
from datetime import date
from dateutil.parser import parse
from gitlab import Gitlab
from gitlab.exceptions import GitlabAuthenticationError
from io import BytesIO
from lxml import etree


logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-8s: %(message)s',
                    datefmt='%H:%M:%S')

ALL = {'per_page': 100, 'page': 0}
LOGIN_PAGE = 'https://neznam.szn.cz/login/'
DAY_PAGE = 'https://neznam.szn.cz/dochazka/detail-dne/%Y/%m/%d'
POST_PAGE = 'https://neznam.szn.cz/sluzby/ulozit'


try:
    username = sys.argv[1]
except IndexError:
    sys.exit('You must pass your username as an argument')

today = date.today()
first_day = today.replace(day=1)

report = {}
password = getpass()
gl = Gitlab('https://gitlab.kancelar.seznam.cz', email=username, password=password)

try:
    gl.auth()
except GitlabAuthenticationError:
    sys.exit('Bad login.')

for project in gl.projects.list(**ALL):
    for branch in project.branches.list(**ALL):
        for commit in project.commits.list(ref_name=branch.name, since=first_day, **ALL):
            author = commit.author_email.split('@', 1)[0]
            if author == username and not commit.message.startswith('Merge '):
                day = parse(commit.committed_date).date()
                work = ' - '.join((project.name, commit.message))
                report.setdefault(day, set()).add(work)
                logging.info('%s - %s' % (day.isoformat(), work.strip()))

if not report:
    logging.info('No work for this month found.')
    sys.exit()

s = requests.Session()

r = s.get(LOGIN_PAGE)
logging.info('Login page loading (%d)' % r.status_code)

content = etree.parse(BytesIO(r.content), etree.HTMLParser())
csrf_token = content.xpath('string(//input[@name="csrfmiddlewaretoken"]/@value)')
logging.debug('CSRF token: %s' % csrf_token)

payload = {
    'username': username,
    'password': password,
    'csrfmiddlewaretoken': csrf_token
}
r = s.post(LOGIN_PAGE, payload)
logging.info('Logging in (%d)' % r.status_code)

employee = None
for day, works in report.items():
    day_url = day.strftime(DAY_PAGE)
    r = s.get(day_url)
    logging.info('Page for %s loading (%d)' % (day.isoformat(), r.status_code))

    content = etree.parse(BytesIO(r.content), etree.HTMLParser())
    csrf_token = content.xpath('string(//input[@name="csrfmiddlewaretoken"]/@value)')
    logging.debug('CSRF token: %s' % csrf_token)

    if employee is None:
        employee = content.xpath('string(//input[@name="employee"]/@value)')
        logging.debug('Employee no.: %s' % employee)

    payload = {
        'day': day.isoformat(),
        'delete_report_new_0': 0,
        'delete_report_new_1': 1,
        'employee': employee,
        'hours_new_0': '*',
        'note_new_0': ''.join(works),
        'project_new_0': 1,
        'project_new_1': 2364,
        'csrfmiddlewaretoken': csrf_token
    }
    r = s.post(POST_PAGE, payload)
    logging.info('Posted report for %s (%d)' % (day.isoformat(), r.status_code))
